module.exports = (app) => {
    const emprestimos = require('../controllers/emprestimo.controller.js');

    // Retrieve all emprestimos
    app.get('/emprestimos', emprestimos.findAll);

    // Retrieve a single emprestimo by id
    app.get('/emprestimos/:id', emprestimos.findOne);

}
