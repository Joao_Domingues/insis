#!/usr/bin/env node
const Service = require('../services/emprestimoService.service.js');
var amqp = require('amqplib/callback_api');

const ge_queue = "GE_Query_Queue";

const exchange = "GE_Query_exchange";

module.exports = {
  executeRabbitReceiver : function() {
    amqp.connect('amqp://192.168.99.100:5672', function(error0, connection) {
      if (error0) {
          throw error0;
      }
      connection.createChannel(function(error1, channel) {
          if (error1) {
              throw error1;
          }

          channel.assertQueue(ge_queue, {
              durable: false
          });

          console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", ge_queue);

          channel.consume(ge_queue, function(msg) {
              var response = JSON.parse(msg.content.toString());
              console.log("[INFO][receiver.js] - Received event of type: " + response.eventType);
              switch(response.eventType) {
                case "EMPRESTIMO_PENDING":
                  Service.createEmprestimoPendente(response);
                  break;
                case "EMPRESTIMO_CREATED":
                  Service.createEmprestimoDefinitivo(response);
                  break;
                case "CLOSE_EMPRESTIMO":
                  Service.closeEmprestimo(response);
                  break;
                default:
                  console.log("[WARNING][receiver.js] - No such event type found.");
              }
          }, {
              noAck: true
          });
      });
    });
  }

}
