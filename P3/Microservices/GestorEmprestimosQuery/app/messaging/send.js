#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
const Emprestimo = require('../models/emprestimo.model.js');

const gr_command_queue = "GR_Command_Queue";
const gr_query_queue = "GR_Query_Queue";
const ga_queue = "GA_Queue";
const gu_command_queue = "GU_Command_Queue";
const gu_query_queue = "GU_Query_Queue";
const ge_command_queue = "GE_Command_Queue";

const exchange = "GE_Query_exchange";

module.exports = {
  sendFanout : function(emprestimo, eventType) {
    amqp.connect('amqp://192.168.99.100:5672', function(error0, connection) {
      if (error0) {
        throw error0;
      }
      connection.createChannel(function(error1, channel) {
        if (error1) {
          throw error1;
        }

        var msg = JSON.stringify(emprestimo);

        // Acrescenta o evenetType ao JSON enviado por mensagem
        msg = msg.substring(0, msg.length-1) + ",\"eventType\":\"" + eventType + "\"}";

        channel.assertExchange(exchange, 'fanout', {
          durable: true
        });

        // binds
        channel.bindQueue(ga_queue, exchange, '');
        channel.bindQueue(gu_command_queue, exchange, '');

        channel.publish(exchange, '', Buffer.from(msg));

        console.log(" [x] Sent %s", msg);
      });

      setTimeout(function() {
        connection.close();
        //process.exit(0);
      }, 500);
    });
  }
}
