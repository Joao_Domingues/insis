const Emprestimo = require('../models/emprestimo.model.js');
const Send = require('../messaging/send.js');

// Retrieve and return all emprestimos from the database.
exports.findAll = (req, res) => {
    //Send.sendFanout("Teste");
    //Send.sendQueue();
    Emprestimo.find()
    .then(emprestimos => {
        res.send(emprestimos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving emprestimos."
        });
    });
};

// Find a single emprestimo with a id
exports.findOne = (req, res) => {
    Emprestimo.findById(req.params.id)
    .then(emprestimo => {
        if(!emprestimo) {
            return res.status(404).send({
                message: "Emprestimo not found with id " + req.params.id
            });
        }
        res.send(emprestimo);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Emprestimo not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Error retrieving emprestimo with id " + req.params.id
        });
    });
};
