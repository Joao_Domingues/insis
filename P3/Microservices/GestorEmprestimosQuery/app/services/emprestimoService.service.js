const Emprestimo = require('../models/emprestimo.model.js');
const Send = require('../messaging/send.js');
const ObjectID = require('mongodb').ObjectID;
const async = require('async');

module.exports = {

  createEmprestimoPendente : function(response) {
    console.log("[INFO][emprestimoServices.services.js - createEmprestimoPendente] - Entrou no createEmprestimoPendente");
    console.log("Emprestimo a ser criado: " + JSON.stringify(response));

    // não se define ID, pois é a primeira vez que se está a guardar o emprestimo na DB
    var emprestimo = new Emprestimo({
        _id: new ObjectID.createFromHexString(response.idEmp), // isto está aqui como placeholder
        inicio: response.inicio,
        fim: response.fim,
        idUtilizador: response.idUtilizador,
        tituloObra: response.tituloObra,
        estado: response.estado
    });

    console.log("[INFO][emprestimoServices.services.js - createEmprestimoPendente] - Emprestimo para ser guardado: " + JSON.stringify(emprestimo));

    // Save Emprestimo in the database
    emprestimo.save()
      .then(data => {
        console.log("[INFO][emprestimoServices.services.js - createEmprestimoPendente] - Emprestimo guardado com sucesso: " + data);
          Send.sendFanout(data, "EMPRESTIMO_CHECK_AUTH"); // to GA

      }).catch(err => {
        console.log("[ERROR][emprestimoServices.services.js - createEmprestimoPendente] - Failed to save Emprestimo in database! " + err.message);
      });

  },

  createEmprestimoDefinitivo : function(response) {

    // aqui define-se o ID à mão, pois este Empréstimo já existe e é para se dar update ao estado
    var emprestimo = new Emprestimo({
        _id: new ObjectID.createFromHexString(response.idEmp),
        inicio: response.inicio,
        fim: response.fim,
        idUtilizador: response.idUtilizador,
        tituloObra: response.tituloObra,
        estado: response.estado
    });

    console.log("[INFO][emprestimoServices.services.js - createEmprestimoDefinitivo] - Emprestimo para ser guardado: " + JSON.stringify(emprestimo));

    // Save Emprestimo in the database
    emprestimo.updateOne({ estado: emprestimo.estado })
      .then(data => {
        console.log("[INFO][emprestimoServices.services.js - createEmprestimoDefinitivo] - Emprestimo guardado com sucesso: " + data);
      }).catch(err => {
        console.log("[ERROR][emprestimoServices.services.js - createEmprestimoDefinitivo] - Failed to save Emprestimo in database!");
      });
  },

  updateEmprestimo : function(emprestimo) {
    console.log("Entrou no updateEmprestimo");
    // TODO
  },

  closeEmprestimo : function(response) {
    console.log("[INFO][emprestimoServices.services.js - closeEmprestimo] - Entrou no closeEmprestimoPendente");

    console.log("response: " + JSON.stringify(response));

    var emprestimo = new Emprestimo({
        _id: new ObjectID.createFromHexString(response.idEmp),
        inicio: response.inicio,
        fim: response.fim,
        idUtilizador: response.idUtilizador,
        tituloObra: response.tituloObra,
        estado: response.estado
    });
    console.log("[INFO][emprestimoServices.services.js - closeEmprestimo] - Emprestimo para ser guardado: " + JSON.stringify(emprestimo));

    var deliveryDate = Date.now();

    // Save Emprestimo in the database
    emprestimo.updateOne({ estado: emprestimo.estado })
      .then(data => {
        // isto está aqui porque eu pensava que dava para adicionar campos livremente em js (e.g.: emprestimo.oldCondition = ...)
        var emprestimoJSON = JSON.stringify(emprestimo);
        emprestimoJSON = emprestimoJSON.substring(0, emprestimoJSON.length-1) + ",\"oldCondition\":\"" + response.oldCondition + "\"}";
        emprestimoJSON = emprestimoJSON.substring(0, emprestimoJSON.length-1) + ",\"newCondition\":\"" + response.newCondition + "\"}";
        var finalEmprestimo = JSON.parse(emprestimoJSON);
        console.log("[INFO][emprestimoServices.services.js - closeEmprestimo] - Emprestimo atualizado com sucesso: " + JSON.stringify(emprestimoJSON));

        if(deliveryDate > emprestimo.fim) {
            Send.sendFanout(finalEmprestimo, "EMPRESTIMO_CLOSED_LATE"); // to GU_C
        } else {
            Send.sendFanout(finalEmprestimo, "EMPRESTIMO_CLOSED_ON_TIME"); // to GU_C
        }
      }).catch(err => {
        console.log("[ERROR][emprestimoServices.services.js - closeEmprestimo] - Failed to update Emprestimo in database!");
      });

  }

}
