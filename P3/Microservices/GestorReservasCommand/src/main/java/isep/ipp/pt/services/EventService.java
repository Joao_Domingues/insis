package isep.ipp.pt.services;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.domain.EventType;
import isep.ipp.pt.domain.Reserva;
import isep.ipp.pt.domain.ReservaState;
import isep.ipp.pt.domain.UserEvent;
import isep.ipp.pt.messaging.RabbitSender;
import isep.ipp.pt.repositories.EventRepository;
import isep.ipp.pt.repositories.ReservaRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class EventService {
	
	private static Log log = LogFactory.getLog(EventService.class);
	
	@Autowired
	EventRepository repository;
	
	@Autowired
	ReservaRepository resRepository;
	
	@Autowired
	RabbitSender rabbitSender;
	
	/**
	 * Sends an Event to GR_Query, to check if the Reserva is free to be scheduled
	 * @param reserva
	 * @throws IOException 
	 */
	
	public void endReserva(String tituloObraUpdate, Integer idUtilizadorUpdate, LocalDateTime dataInicioUpdate, LocalDateTime dataFimUpdate) {
		Reserva res = null;
		List<Reserva> reservas = new ArrayList<Reserva>();
		resRepository.findAll().forEach(reserva -> reservas.add(reserva));		
		System.out.println("Data Inicio Update: " + dataInicioUpdate);
		System.out.println("Data Fim Update: " + dataFimUpdate);
		
		LocalDate dataInicio = dataInicioUpdate.toLocalDate();
		LocalDate dataFim = dataFimUpdate.toLocalDate();
		
		for(Reserva r : reservas) {
			if(r.getState().equals(ReservaState.UP_TO_DATE)) {
				LocalDate rInicio = r.getInicio().toLocalDate();
				LocalDate rFim = r.getFim().toLocalDate();
				System.out.println("Datas Emprestimo: " + dataInicio.toString() + "   ----   " + dataFim.toString());
				System.out.println("Datas Reserva: " + rInicio.toString() + "   ----   " + rFim.toString());
				
				System.out.println("Datas Emprestimo HC: + " + dataInicio.hashCode() + "  ---  " + dataFim.hashCode());
				System.out.println("Datas Reserva HC: + " + rInicio.hashCode() + "  ---  " + rFim.hashCode());
				
				
				System.out.println(dataInicio.hashCode() == rInicio.hashCode());
				System.out.println(dataFim.hashCode() == rFim.hashCode());
				
				if(rInicio.hashCode() == dataInicio.hashCode() && rFim.hashCode() == dataFim.hashCode() && r.getTituloObra().compareToIgnoreCase(tituloObraUpdate)==0 && r.getIdUtilizador() == idUtilizadorUpdate) {
					res = r;
				}
			}
		}
		
		System.out.println("Encontrei Reserva: " + res);
		if (res == null) return;
		
		res.setState(ReservaState.END);
		
		UserEvent event = new UserEvent(LocalDateTime.now(), EventType.END_RESERVA, res);
		repository.save(event);
		
		log.info("After save: " + res);
		
		rabbitSender.sendFanout(event);
	}
	
	
	public Reserva requireCreation(Reserva reserva) {
		
		if(utilizadorSuspensoOuInativo(reserva.getIdUtilizador())) return null;
		List<UserEvent> events = new ArrayList<UserEvent>(); 
				
		//Seleciona eventos para a mesma obra que ja nao estejam num primeiro estado de transição
		repository.findAll().forEach(ev -> {
			if(ev.getData().getTituloObra().compareToIgnoreCase(reserva.getTituloObra())==0 && ev.getId() != 0) {
				if(reserva.getIdRes() != ev.getData().getIdRes()) {
					events.add(ev);
				}
			}
		});
				
		if(events.isEmpty()) {
			log.info("No events with the same title as Reserva found.");
		}
		
		log.info("Events with the same title as Reserva found.");
		
		Map<Integer, UserEvent> map = new HashMap<Integer, UserEvent>(); 
		events.forEach(ev -> {

			if(map.containsKey(ev.getData().getIdRes())) {
				if(ev.getTimestamp().isAfter(map.get(ev.getData().getIdRes()).getTimestamp())) {
					map.replace(ev.getData().getIdRes(), ev);
				}
			} else {
				map.put(ev.getData().getIdRes(), ev);
			}
			
				
		});
		
		log.info("Map created with " + map.size() + " elements");
	
		for(Map.Entry<Integer, UserEvent> entry : map.entrySet()) {
			log.info(entry);
			UserEvent ev = entry.getValue();
			if(intersecaoData(reserva, ev.getData().getInicio(), ev.getData().getFim())) {
				
				if(ev.getData().getState() == ReservaState.PENDING || ev.getData().getState() == ReservaState.UP_TO_DATE) {
					log.info("Intersection found");
					return null;	
				}
			}
		}
		
		log.info("No intersections found.");
		
		//if(intersectsEmprestimo(reserva)) return null;
		
		List<Reserva> reservas = new ArrayList<Reserva>();
		resRepository.findAll().forEach(r -> reservas.add(r));
		
		Set<Reserva> unique = new HashSet<Reserva>(reservas);
		reserva.setIdRes(unique.size()+1);
		
		Reserva res = resRepository.save(reserva);
		UserEvent event = new UserEvent(LocalDateTime.now(), EventType.RESERVA_PENDING, res);
		repository.save(event);
		
		log.info("After save: " + res);
		
		rabbitSender.sendFanout(event);
		return res;
	}
	
	private boolean utilizadorSuspensoOuInativo(int idUtilizador) {
		try {
			URL utilizadorURL = new URL("http://localhost:8080/utilizadores/" + idUtilizador);
			System.out.println("URL: " + utilizadorURL);
			
			HttpURLConnection utilizadorCon;
				
			utilizadorCon = (HttpURLConnection) utilizadorURL.openConnection();
			utilizadorCon.setRequestMethod("GET");
			StringBuffer utilizadorContent = new StringBuffer();
			BufferedReader in = new BufferedReader(new InputStreamReader(utilizadorCon.getInputStream()));
			String inputLine;
							
			while ((inputLine = in.readLine()) != null) {
				utilizadorContent.append(inputLine);
			}			
			in.close();
			Gson g = new Gson();
			
			JsonObject json = g.fromJson(utilizadorContent.toString(), JsonObject.class);
			
			String estadoUtilizador =  json.get("estado").getAsString();
			
			return estadoUtilizador.compareToIgnoreCase("ATIVO") != 0;
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public void canCreateReserva(Reserva reserva) {
		reserva.setState(ReservaState.UP_TO_DATE);
		Reserva res = resRepository.save(reserva);
		UserEvent event = new UserEvent(LocalDateTime.now(), EventType.CREATE_RESERVA, res);
		repository.save(event);
		rabbitSender.sendFanout(event);
	}
	
	public void cannotCreateReserva(Reserva reserva) {
		Reserva res = resRepository.save(reserva);
		UserEvent event = new UserEvent(LocalDateTime.now(), EventType.CANCELED_RESERVA, res);
		UserEvent ev = repository.save(event);
		rabbitSender.sendFanout(event);
	}
	
	private boolean intersectsEmprestimo(Reserva reserva) {
		try {
			URL emprestimoURL = new URL("http://localhost:4000/emprestimos");
			
			HttpURLConnection emprestimoCon;
				
			emprestimoCon = (HttpURLConnection) emprestimoURL.openConnection();
			emprestimoCon.setRequestMethod("GET");
			StringBuffer emprestimoContent = new StringBuffer();
			BufferedReader in = new BufferedReader(new InputStreamReader(emprestimoCon.getInputStream()));
			String inputLine;
							
			while ((inputLine = in.readLine()) != null) {
				emprestimoContent.append(inputLine);
			}
							
			in.close();
					
			List<String> bufferSplit = specialSplit(emprestimoContent.toString());
			Gson g = new Gson();
			for(String s : bufferSplit) {
				JsonObject json = g.fromJson(s, JsonObject.class);
				String tituloObra =  json.get("tituloObra").getAsString();
				LocalDateTime dataInicio = conversaoData( json.get("inicio").getAsString());
				LocalDateTime dataFim =  conversaoData( json.get("fim").getAsString());
				if(tituloObra.compareToIgnoreCase(reserva.getTituloObra())==0) {
					if(intersecaoData(reserva, dataInicio, dataFim)) return true;
				}
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		} 
							
		return false;

	}
	
	private LocalDateTime conversaoData(String data) {
		String[] info = data.split("-");
		return LocalDateTime.of(Integer.parseInt(info[0]), Integer.parseInt(info[1]), Integer.parseInt(info[2].substring(0, 2)), 12, 00);
	}
	
	private boolean intersecaoData(Reserva atual, LocalDateTime dataInicio, LocalDateTime dataFim) {
		if(dataInicio.isBefore(atual.getFim()) && dataInicio.isAfter(atual.getInicio())) return true;
		if(atual.getInicio().isBefore(dataFim) && atual.getInicio().isAfter(dataInicio)) return true;
		if(atual.getInicio().hashCode()==dataInicio.hashCode() || atual.getFim().hashCode()==dataFim.hashCode()) return true;
		return false;
	}
	
	private List<String> specialSplit(String toSplit) {
		List<String> split = new ArrayList<String>();
		int currentIndex = 0, level = 0, lastIndex = 0;
		String section;
				
		while(currentIndex < toSplit.length()) {
			if(toSplit.charAt(currentIndex) == '{') {
				if(level == 0) lastIndex = currentIndex+1;
				level++;
			}
			
			if(toSplit.charAt(currentIndex) == '}') {
				level--;
				if(level == 0) {
					section = toSplit.substring(lastIndex, currentIndex);
					if(!section.contains("error=Obra not found")) {
						split.add("{" + section + "}");
					}	
				}
			}
			currentIndex++;
		}
		
		
		return split;
	}


	
	
}
