package isep.ipp.pt.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Reserva {

	@Id
	@GeneratedValue
	private int id;
	private int idRes;
	private LocalDateTime inicio;
	private LocalDateTime fim;
	private int idUtilizador;
	private String tituloObra;
	private ReservaState state;
	
	public Reserva() {
		
	}
	
	public Reserva(int idRes, LocalDateTime inicio, LocalDateTime fim, int idUtilizador, String tituloObra, ReservaState state) {
		this.idRes = idRes;
		this.inicio = inicio;
		this.fim = fim;
		this.idUtilizador = idUtilizador;
		this.tituloObra = tituloObra;
		this.state = state;
	}
	
	public Reserva(LocalDateTime inicio, LocalDateTime fim, int idUtilizador, String tituloObra) {
		this.idRes = 0;
		this.inicio = inicio;
		this.fim = fim;
		this.idUtilizador = idUtilizador;
		this.tituloObra = tituloObra;
		this.state = ReservaState.PENDING;
	}
	
	public String getTituloObra() {
		return tituloObra;
	}

	public void setTituloObra(String tituloObra) {
		this.tituloObra = tituloObra;
	}

	public ReservaState getState() {
		return state;
	}

	public void setState(ReservaState state) {
		this.state = state;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setIdRes(int idRes) {
		this.idRes = idRes;
	}
	
	public int getIdRes() {
		return this.idRes;
	}
	
	public LocalDateTime getInicio() {
		return this.inicio;
	}
	
	public LocalDateTime getFim() {
		return this.fim;
	}
	
	public int getIdUtilizador() {
		return this.idUtilizador;
	}
	
	public void setInicio(LocalDateTime inicio) {
		this.inicio = inicio;
	}
	
	public void setFim(LocalDateTime fim) {
		this.fim = fim;
	}
	
	public void setIdUtilizador(int idUtilizador) {
		this.idUtilizador = idUtilizador;
	}
	
	
	@Override
	public String toString() {
		return "\nid: " + this.id + 
				"\ninicio: " + this.inicio.toString() + 
				"\nfim: " + this.fim.toString() + 
				"\niduser: " + this.idUtilizador + 
				"\ntitulo: " + this.tituloObra +
				"\nstate: " + this.state;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + idRes;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reserva other = (Reserva) obj;
		if (id != other.id)
			return false;
		if (idRes != other.idRes)
			return false;
		return true;
	}
	
	
	
}
