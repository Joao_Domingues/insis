package isep.ipp.pt.messaging;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class RabbitConfig {

	@Value("${fanout.exchange}")
	private String fanoutExchange;

	@Value("${gr_query_queue.name}")
	private String grQueryQueueName;

	@Bean
	Queue grQueryQueue() {
		return new Queue(grQueryQueueName, false);
	}

	@Bean
	FanoutExchange exchange() {
		return new FanoutExchange(fanoutExchange);
	}

    @Bean
    public Binding binding1(FanoutExchange fanout) {
        return BindingBuilder.bind(grQueryQueue()).to(fanout);
    }

	@Profile("receiver")
	@Bean
	public RabbitReceiver receiver() {
		return new RabbitReceiver();
	}

	@Profile("sender")
	@Bean
	public RabbitSender sender() {
		return new RabbitSender();
	}
}