package isep.ipp.pt.domain;

public enum ReservaState implements ValueObject {
	PENDING {
		@Override
		public String toString() {
			return "PENDING";
		}
	},
	UP_TO_DATE {
		@Override
		public String toString() {
			return "UP_TO_DATE";
		}
	},
	CANCELED {
		@Override
		public String toString() {
			return "CANCELED";
		}
	},
	END {
		@Override
		public String toString() {
			return "END";
		}
	}
}
