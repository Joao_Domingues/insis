package isep.ipp.pt.repositories;

import org.springframework.data.repository.CrudRepository;

import isep.ipp.pt.domain.Reserva;

public interface ReservaRepository extends CrudRepository<Reserva, Integer> { }
