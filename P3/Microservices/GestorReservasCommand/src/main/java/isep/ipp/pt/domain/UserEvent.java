package isep.ipp.pt.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class UserEvent {
	
	@Id
	@GeneratedValue
	private int id;
	private LocalDateTime timestamp;
	private EventType eventType;
	@OneToOne
	private Reserva data;
	
	public UserEvent( ) {
		
	}
	
	public UserEvent(LocalDateTime timestamp, EventType eventType, Reserva data) {
		this.timestamp = timestamp;
		this.eventType = eventType;
		this.data = data;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public Reserva getData() {
		return data;
	}

	public void setData(Reserva data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "id: " + this.id + " \ntimestamp: " + this.timestamp.toString() + " \nEvent Type: " + this.eventType + " \nReserva: " + this.data.toString();
	}
}
