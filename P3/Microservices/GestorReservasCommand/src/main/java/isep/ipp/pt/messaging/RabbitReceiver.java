package isep.ipp.pt.messaging;

import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.domain.Reserva;
import isep.ipp.pt.domain.ReservaState;
import isep.ipp.pt.services.EventService;

@Component
public class RabbitReceiver {
	
	private static Log log = LogFactory.getLog(RabbitReceiver.class);
	
	@Autowired
	EventService service;
	
	@Value("${gr_command_queue.name}")
	private String grQueueName;
	
	@Bean
	Queue grQueue() {
		return new Queue(grQueueName, false);
	}
	
	@RabbitListener(queues = "${gr_command_queue.name}")
    public void receive(String in) {
		
		Gson g = new Gson();
		
		JsonObject json = g.fromJson(in, JsonObject.class);
		String type = json.get("eventType").getAsString();
		
		log.info("json received: " + json);
		
		log.info("[X] Received " + type);
		

		switch(type) {
			case "HAS_AUTHORIZATION_RESERVA":
				log.info("JSON " + json);
				Integer id = json.get("idRes").getAsInt();
				log.info("preTitulo");
				String tituloObra =  json.get("tituloObra").getAsString();
				log.info("posTitulo");
				Integer idUtilizador =  json.get("idUtilizador").getAsInt();
				LocalDateTime dataInicio = LocalDateTime.parse(json.get("inicio").getAsString());
				LocalDateTime dataFim = LocalDateTime.parse(json.get("fim").getAsString());
				
				ReservaState state = ReservaState.valueOf(ReservaState.class, json.get("state").getAsString());
				
				Reserva r = new Reserva(id, dataInicio, dataFim, idUtilizador, tituloObra, state);
				
				service.canCreateReserva(r);
				break;
			case "ERROR_AUTHORIZATION_RESERVA" :
				Integer idError = json.get("idRes").getAsInt();
				String tituloObraError =  json.get("tituloObra").getAsString();
				Integer idUtilizadorError =  json.get("idUtilizador").getAsInt();
				LocalDateTime dataInicioError = LocalDateTime.parse(json.get("inicio").getAsString());
				LocalDateTime dataFimError = LocalDateTime.parse(json.get("fim").getAsString());
				ReservaState errorState = ReservaState.CANCELED;
				Reserva errorRes = new Reserva(idError, dataInicioError, dataFimError, idUtilizadorError, tituloObraError, errorState);
				
				service.cannotCreateReserva(errorRes);
				break;
			case "EMPRESTIMO_CHANGE_RESERVA_STATE" :
				log.info("JSON RECEIVED: " + json);
				String tituloObraUpdate =  json.get("tituloObra").getAsString();
				Integer idUtilizadorUpdate =  json.get("idUtilizador").getAsInt();
				LocalDateTime dataInicioUpdate = conversaoData(json.get("inicio").getAsString());
				LocalDateTime dataFimUpdate = conversaoData(json.get("fim").getAsString());
				
				service.endReserva(tituloObraUpdate, idUtilizadorUpdate, dataInicioUpdate, dataFimUpdate);
				break;
			default:
				log.info("Event Type not found " + type);
		}
    }
	
	private LocalDateTime conversaoData(String data) {
		System.out.println("Data a converter: " + data);
		String[] info = data.split("-");
		return LocalDateTime.of(Integer.parseInt(info[0]), Integer.parseInt(info[1]), Integer.parseInt(info[2].substring(0, 2)), 12, 00);
	}
	
}
