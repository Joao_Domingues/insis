package isep.ipp.pt.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import isep.ipp.pt.domain.UserEvent;

@Service
public class RabbitSender {
	
	private static Log log = LogFactory.getLog(RabbitSender.class);
	
	@Autowired
	private RabbitTemplate template;

	@Value("${fanout.exchange}")
	private String fanoutExchange;

	public void sendFanout(UserEvent event) {
		Gson g = new Gson();
		
		String json = g.toJson(event);
		
		this.template.setExchange(fanoutExchange);
		this.template.convertAndSend(json);
		log.info(" [x] Sent '" + event.getEventType() + "'");
	}
}
