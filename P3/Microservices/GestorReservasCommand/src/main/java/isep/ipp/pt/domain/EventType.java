package isep.ipp.pt.domain;

public enum EventType implements ValueObject {
	RESERVA_PENDING {
		@Override
		public String toString() {
			return "RESERVA_PENDING";
		}
	},
	CREATE_RESERVA {
		@Override
		public String toString() {
			return "CREATE_RESERVA";
		}
	},
	CANCELED_RESERVA {
		@Override
		public String toString() {
			return "CANCELED_RESERVA";
		}
	},
	END_RESERVA {
		@Override
		public String toString() {
			return "END_RESERVA";
		}
	}
}
