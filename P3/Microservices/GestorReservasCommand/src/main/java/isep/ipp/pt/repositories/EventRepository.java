package isep.ipp.pt.repositories;

import org.springframework.data.repository.CrudRepository;

import isep.ipp.pt.domain.UserEvent;

public interface EventRepository extends CrudRepository<UserEvent, Integer> { }
