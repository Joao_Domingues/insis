package isep.ipp.pt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import isep.ipp.pt.domain.Reserva;
import isep.ipp.pt.domain.ReservaState;
import isep.ipp.pt.messaging.RabbitSender;
import isep.ipp.pt.services.EventService;

@RestController
public class ReservaController {

	@Autowired
	EventService service;
	
	@Autowired
	RabbitSender rabbitSender;

	@PostMapping("/reservas")
	private Reserva createReserva(@RequestBody Reserva reserva) {
		reserva.setState(ReservaState.PENDING);
		return service.requireCreation(reserva);
	}
}