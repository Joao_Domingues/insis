package isep.ipp.pt.domain;

import java.io.Serializable;

public interface ValueObject extends Serializable {

	@Override
	String toString();

	@Override
	boolean equals(Object other);

	@Override
	int hashCode();

}
