package isep.ipp.pt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestorUtilizadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestorUtilizadorApplication.class, args);
	}

}
