package isep.ipp.pt.messaging;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitSender {
	@Autowired
	private RabbitTemplate template;

	@Value("${fanout.exchange}")
	private String fanoutExchange;

	public void sendFanout() {
		String message = "Fanout Message from GestorUtilizadores!";
		this.template.setExchange(fanoutExchange);
		this.template.convertAndSend(message);
		System.out.println(" [x] Sent '" + message + "'");
	}
	
	
}
