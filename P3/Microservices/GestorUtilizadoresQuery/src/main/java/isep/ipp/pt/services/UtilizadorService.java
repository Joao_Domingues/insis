package isep.ipp.pt.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import isep.ipp.pt.domain.Utilizador;
import isep.ipp.pt.repositories.*;

@Service
public class UtilizadorService {

	private static Log log = LogFactory.getLog(UtilizadorService.class);
	
	@Autowired
	UtilizadorRepository utilizadorRepository;

	public List<Utilizador> getAllUtilizadores() {
		List<Utilizador> persons = new ArrayList<Utilizador>();
		utilizadorRepository.findAll().forEach(person -> persons.add(person));
		return persons;
	}

	public Utilizador getUtilizadorById(int id) {
		return utilizadorRepository.findById(id).get();
	}
	
	public void updateUtilizador(Utilizador utilizador) {
		Utilizador u = utilizadorRepository.save(utilizador);
		log.info("Fim do UC Fecho de Empréstimo/Update Utilizador: " + u);
	}
	
	public void createUtilizador(Utilizador utilizador) {
		Utilizador u = utilizadorRepository.save(utilizador);
		log.info("Fim do UC Criação de Utilizador: " + u);
	}

}
