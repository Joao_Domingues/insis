package isep.ipp.pt.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import isep.ipp.pt.domain.Utilizador;
import isep.ipp.pt.messaging.RabbitSender;
import isep.ipp.pt.services.UtilizadorService;

@RestController
public class UtilizadorController {

	@Autowired
	UtilizadorService utilizadorService;
	
	@Autowired
	RabbitSender rabbitSender;

	@GetMapping("/utilizadores")
	private List<Utilizador> getAllUtilizadores() {
		return utilizadorService.getAllUtilizadores();
	}
	
	@GetMapping("/utilizadores/{id}/reputacao")
	private Double getUtilizadorInfo(@PathVariable("id") int id) {
		//return utilizadorService.getUtilizadorById(id).getReputacao().getValor();
		return 2.5;
	}

	@GetMapping("/utilizadores/{id}")
	private Utilizador getUtilizador(@PathVariable("id") int id) {
		return utilizadorService.getUtilizadorById(id);
	}
	
}