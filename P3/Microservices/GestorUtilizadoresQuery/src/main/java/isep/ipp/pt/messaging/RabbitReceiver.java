package isep.ipp.pt.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.domain.Utilizador;
import isep.ipp.pt.services.UtilizadorService;

@Component
public class RabbitReceiver {

	private static Log log = LogFactory.getLog(RabbitReceiver.class);
	
	@Autowired
	UtilizadorService service;
	
	@Value("${gu_query_queue.name}")
	private String guQueueName;
	
	@Bean
	Queue guQueue() {
		return new Queue(guQueueName, false);
	}
	
	@RabbitListener(queues = "${gu_query_queue.name}")
    public void receive(String in) {
		Gson g = new Gson();
		
		JsonObject json = g.fromJson(in, JsonObject.class);
		Utilizador utilizador = g.fromJson(json.get("data"), Utilizador.class);
		JsonObject resData = g.fromJson(json.get("data"), JsonObject.class);
		int realIdUsr = resData.get("idUsr").getAsInt();
		utilizador.setId(realIdUsr);
		
		String type = json.get("eventType").getAsString();
		log.info("[X] Received " + type);
		
		switch(type) {
			case "CREATE_USER":
				service.createUtilizador(utilizador);
				break;
			case "UPDATE_USER":
				service.updateUtilizador(utilizador);
				break;
			default:
				log.info("Event Type not found " + type);
		}
    }
	
}
