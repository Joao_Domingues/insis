package isep.ipp.pt.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Utilizador {

	@Id
	private int id;
	private String nome;
	private Estado estado;
	private double reputacao;
	private LocalDateTime ultimaSuspensao;

	public Utilizador() {

	}

	public Utilizador(String nome) {
		this.nome = nome;
		this.estado = Estado.ATIVO;
		this.reputacao = 2.5;
		this.ultimaSuspensao = LocalDateTime.MIN;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public void setUltimaSuspensao(LocalDateTime ultimaSuspensao) {
		this.ultimaSuspensao = ultimaSuspensao;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEstado() {
		return estado.toString();
	}

	public double getReputacao() {
		return reputacao;
	}

	public void setReputacao(double reputacao) {
		this.reputacao = reputacao;
	}
	
	public LocalDateTime getUltimaSuspensao() {
		return this.ultimaSuspensao;
	}
	
}
