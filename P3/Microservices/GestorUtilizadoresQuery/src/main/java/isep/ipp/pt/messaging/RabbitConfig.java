package isep.ipp.pt.messaging;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class RabbitConfig {

	@Value("${fanout.exchange}")
	private String fanoutExchange;
	
	@Bean
	FanoutExchange exchange() {
		return new FanoutExchange(fanoutExchange);
	}

	@Profile("receiver")
	@Bean
	public RabbitReceiver receiver() {
		return new RabbitReceiver();
	}

	@Profile("sender")
	@Bean
	public RabbitSender sender() {
		return new RabbitSender();
	}
}