package isep.ipp.pt.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.domain.Estado;
import isep.ipp.pt.domain.EventType;
import isep.ipp.pt.domain.UserEvent;
import isep.ipp.pt.domain.Utilizador;
import isep.ipp.pt.messaging.RabbitSender;
import isep.ipp.pt.repositories.*;

@Service
public class EventService {
	
	private static Log log = LogFactory.getLog(EventService.class);

	@Autowired
	EventRepository repository;
	
	@Autowired
	RabbitSender sender;
	
	@Autowired
	EventRepository eventRepository;
	
	@Autowired
	UtilizadorRepository utilizadorRepository;
	
	public boolean updateReputacaoUtilizador(int id, double newReputacao) {
		Utilizador utilizador = getUtilizadorById(id);
		if(utilizador == null) return false;
		utilizador.setReputacao(newReputacao);
		utilizador.setEstado(Estado.ATIVO);
		Utilizador ut = utilizadorRepository.save(utilizador);
		UserEvent ue = new UserEvent(LocalDateTime.now(), EventType.UPDATE_USER, ut);
		eventRepository.save(ue);
		
		sender.sendFanout(ue);
		return true;
		
	}
	
	
	public boolean updateEstadoUtilizador(int id, String estado) {
		Estado newEstado;
		if(estado.compareToIgnoreCase(Estado.ATIVO.toString())==0) {
			newEstado = Estado.ATIVO;
		} else if (estado.compareToIgnoreCase(Estado.INATIVO.toString())==0) {
			newEstado = Estado.INATIVO;
		} else if (estado.compareToIgnoreCase(Estado.SUSPENSO.toString())==0){
			newEstado = Estado.SUSPENSO;
		} else {
			return false;
		}
		Utilizador utilizador = getUtilizadorById(id);
		if(utilizador == null) return false;
		utilizador.setEstado(newEstado);
		
		Utilizador ut = utilizadorRepository.save(utilizador);
		UserEvent ue = new UserEvent(LocalDateTime.now(), EventType.UPDATE_USER, ut);
		eventRepository.save(ue);
		
		sender.sendFanout(ue);
		return true;
		
	}
	
	public Utilizador requireCreation(Utilizador utilizador) {
		List<Utilizador> persons = new ArrayList<Utilizador>();
		utilizadorRepository.findAll().forEach(person -> persons.add(person));
		
		Set<Utilizador> unique = new HashSet<Utilizador>(persons);
		utilizador.setIdUsr(unique.size()+1);
		
		Utilizador ut = utilizadorRepository.save(utilizador);
		System.out.println("Utilizador: " + ut);
		UserEvent ue = new UserEvent(LocalDateTime.now(), EventType.CREATE_USER, ut);
		eventRepository.save(ue);
		
		sender.sendFanout(ue);
		return ut;
	}

	public void updateUserWithTimePenalty(int idUtilizador, double oldCondition, double newCondition) {
		Utilizador utilizador = getUtilizadorById(idUtilizador);
		
		if(utilizador == null) {
			log.info("Utilizador com id " + idUtilizador + " não existe.");
			return;
		}

		if(oldCondition == newCondition) {
			utilizador.processarDevolucao(newCondition, "atrasado");
		} else {
			utilizador.processarDevolucao(newCondition, "atrasado");
			utilizador.processarDevolucao(newCondition, "obra alterada");
		}
		
		utilizador = utilizadorRepository.save(utilizador);
		UserEvent event = new UserEvent(LocalDateTime.now(), EventType.UPDATE_USER, utilizador);
		
		event = eventRepository.save(event);
		log.info("depois save ");
		sender.sendFanout(event);
	}
	
	public void updateUserWithoutTimePenalty(int idUtilizador, double oldCondition, double newCondition) {
		Utilizador utilizador = getUtilizadorById(idUtilizador);
		
		if(utilizador == null) {
			log.info("Utilizador com id " + idUtilizador + " não existe.");
			return;
		}
		
		if(oldCondition == newCondition) {
			utilizador.processarDevolucao(newCondition, "standard");
		} else {
			utilizador.processarDevolucao(newCondition, "obra alterada");
		}
		
		utilizador = utilizadorRepository.save(utilizador);
		
		UserEvent event = new UserEvent(LocalDateTime.now(), EventType.UPDATE_USER, utilizador);
		
		event = eventRepository.save(event);
		
		sender.sendFanout(event);
	}
	
	private Utilizador getUtilizadorById(int idUtilizador) {
		/*
		List<UserEvent> events = new ArrayList<UserEvent>(); 
		
		repository.findAll().forEach(ev -> {
			if(ev.getData().getIdUsr() == idUtilizador) events.add(ev);
		});
		
		if(events.isEmpty()) {
			return null;
		}

		UserEvent toReturn = events.get(0);
		
		for(UserEvent ev : events) {
			if(ev.getTimestamp().isAfter(toReturn.getTimestamp())) {
				toReturn = ev;
			}
		}
		
		Utilizador oldUser = toReturn.getData();
		Utilizador newUser = new Utilizador(oldUser.getIdUsr(), oldUser.getNome(), oldUser.getEstado(), oldUser.getReputacao(), oldUser.getUltimaSuspensao());
		return newUser;*/

		
		try {
			URL utilizadorURL = new URL("http://localhost:8080/utilizadores/" + idUtilizador);
			
			HttpURLConnection utilizadorCon;
				
			utilizadorCon = (HttpURLConnection) utilizadorURL.openConnection();
			utilizadorCon.setRequestMethod("GET");
			StringBuffer utilizadorContent = new StringBuffer();
			BufferedReader in = new BufferedReader(new InputStreamReader(utilizadorCon.getInputStream()));
			String inputLine;
							
			while ((inputLine = in.readLine()) != null) {
				utilizadorContent.append(inputLine);
			}			
			in.close();
			Gson g = new Gson();
			
			JsonObject json = g.fromJson(utilizadorContent.toString(), JsonObject.class);
			int id = json.get("id").getAsInt();
			String nome = json.get("nome").getAsString();
			Estado estado =  Estado.valueOf(json.get("estado").getAsString());
			double reputacao = json.get("reputacao").getAsDouble();
			LocalDateTime ultimaSuspensao = LocalDateTime.parse(json.get("ultimaSuspensao").getAsString());
			
			return new Utilizador(id, nome, estado, reputacao, ultimaSuspensao);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	private LocalDateTime conversaoData(String data) {
		String[] info = data.split("-");
		return LocalDateTime.of(Integer.parseInt(info[0]), Integer.parseInt(info[1]), Integer.parseInt(info[2].substring(0, 2)), 12, 00);
	}
	
	private List<String> specialSplit(String toSplit) {
		List<String> split = new ArrayList<String>();
		int currentIndex = 0, level = 0, lastIndex = 0;
		String section;
				
		while(currentIndex < toSplit.length()) {
			if(toSplit.charAt(currentIndex) == '{') {
				if(level == 0) lastIndex = currentIndex+1;
				level++;
			}
			
			if(toSplit.charAt(currentIndex) == '}') {
				level--;
				if(level == 0) {
					section = toSplit.substring(lastIndex, currentIndex);
					if(!section.contains("error=Obra not found")) {
						split.add("{" + section + "}");
					}	
				}
			}
			currentIndex++;
		}
		
		
		return split;
	}

}
