package isep.ipp.pt.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Utilizador {

	@Id
	@GeneratedValue
	private int id;
	private int idUsr;
	private String nome;
	private Estado estado;
	private double reputacao;
	private LocalDateTime ultimaSuspensao;

	public Utilizador() {

	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public void setUltimaSuspensao(LocalDateTime ultimaSuspensao) {
		this.ultimaSuspensao = ultimaSuspensao;
	}

	public Utilizador(int idUsr, String nome, Estado estado, double reputacao, LocalDateTime ultimaSuspensao) {
		this.idUsr = idUsr;
		this.nome = nome;
		this.estado = estado;
		this.reputacao = reputacao;
		this.ultimaSuspensao = ultimaSuspensao;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdUsr() {
		return idUsr;
	}

	public void setIdUsr(int idUsr) {
		this.idUsr = idUsr;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public double getReputacao() {
		return reputacao;
	}

	public void setReputacao(double reputacao) {
		this.reputacao = reputacao;
		if(this.reputacao > 6.0) this.reputacao = 6.0;
	}
	
	public LocalDateTime getUltimaSuspensao() {
		return this.ultimaSuspensao;
	}
	
	public void processarDevolucao(double conservacao, String devolucao) {
		this.atualizarReputacao(conservacao, devolucao);
		
		if(this.getReputacao() < 1) {
			System.out.println("Reputação desceu abaixo do 1.");
			this.estado = Estado.INATIVO;
		} else if(this.getReputacao() < 2) {
			System.out.println("Reputação desceu abaixo dos 2.");
			this.estado = Estado.SUSPENSO;
			this.ultimaSuspensao = LocalDateTime.now();
		}
	}
	
	public void reativar(double novoValor) {
		this.estado = Estado.ATIVO;
		this.setReputacao(novoValor);
	}
	
	private void atualizarReputacao(double conservacao, String devolucao) {
		switch(devolucao) {
			case "atrasado":
				devolucaoAtrasada(conservacao);
				break;
			case "standard":
				devolucaoATempo(conservacao);
				break;
			case "obra alterada":
				devolucaoAlterada(conservacao);
				break;
			default:
				break;
		}
	}
	
	private void devolucaoATempo(double conservacao) {
		this.reputacao += 0.1 * conservacao;
		if(this.reputacao > 6) this.reputacao = 6;
	}
	
	private void devolucaoAtrasada(double conservacao) {
		this.reputacao -= 0.2 * (6-conservacao);
		if(this.reputacao < 0) this.reputacao = 0;
	}
	
	private void devolucaoAlterada(double conservacao) {
		this.reputacao -= 0.25 * (6-conservacao);
		if(this.reputacao < 0) this.reputacao = 0;
	}

	@Override
	public String toString() {
		return "\nid: " + this.id + 
				"\nidUsr: " + this.getIdUsr() + 
				"\nnome: " + this.getNome() + 
				"\nestado: " + this.getEstado().toString() + 
				"\nreputacao: " + this.getReputacao() + 
				"\nultimaSuspensao: " + this.getUltimaSuspensao() ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + idUsr;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Utilizador other = (Utilizador) obj;
		if (id != other.id)
			return false;
		if (idUsr != other.idUsr)
			return false;
		return true;
	}
	
}
