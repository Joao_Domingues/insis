package isep.ipp.pt.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import isep.ipp.pt.domain.Estado;
import isep.ipp.pt.domain.EventType;
import isep.ipp.pt.domain.UserEvent;
import isep.ipp.pt.domain.Utilizador;
import isep.ipp.pt.messaging.RabbitSender;
import isep.ipp.pt.services.EventService;

@RestController
public class UtilizadorController {

	@Autowired
	EventService service;
	
	@Autowired
	RabbitSender rabbitSender;

	@PostMapping("/utilizadores")
	private Utilizador saveUtilizador(@RequestBody Utilizador utilizador) {
		utilizador.setEstado(Estado.ATIVO);
		utilizador.setReputacao(2.5);
		utilizador.setUltimaSuspensao(LocalDateTime.of(2000, 1, 1, 0, 0));
		return service.requireCreation(utilizador);
	}
	
	@PutMapping("/utilizadores/{id}")
	private boolean reativarUtilizador(@PathVariable("id") int id, @RequestBody Utilizador utilizador) {
		System.out.println("utilizador.rep: " + utilizador.getReputacao());
		return service.updateReputacaoUtilizador(id, utilizador.getReputacao());
	}

}