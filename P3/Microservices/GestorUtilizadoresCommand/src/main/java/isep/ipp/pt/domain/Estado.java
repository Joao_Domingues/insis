package isep.ipp.pt.domain;

public enum Estado implements ValueObject {
	ATIVO {
		@Override
		public String toString() {
			return "ATIVO";
		}
	},
	INATIVO {
		@Override
		public String toString() {
			return "INATIVO";
		}
	},
	SUSPENSO {
		@Override
		public String toString() {
			return "SUSPENSO";
		}
	}
}
