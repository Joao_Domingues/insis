package isep.ipp.pt.domain;

public enum EventType implements ValueObject {
	CREATE_USER {
		@Override
		public String toString() {
			return "CREATE_USER";
		}
	},
	UPDATE_USER {
		@Override
		public String toString() {
			return "UPDATE_USER";
		}
	}
}
