package isep.ipp.pt.messaging;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class RabbitConfig {

	@Value("${fanout.exchange}")
	private String fanoutExchange;

	@Value("${gu_query_queue.name}")
	private String guQueryQueueName;

	@Bean
	Queue gu_Query_Queue() {
		return new Queue(guQueryQueueName, false);
	}

	@Bean
	FanoutExchange exchange() {
		return new FanoutExchange(fanoutExchange);
	}

	@Bean
    public Binding binding1(FanoutExchange fanout) {
        return BindingBuilder.bind(gu_Query_Queue()).to(fanout);
    }

	@Profile("receiver")
	@Bean
	public RabbitReceiver receiver() {
		return new RabbitReceiver();
	}

	@Profile("sender")
	@Bean
	public RabbitSender sender() {
		return new RabbitSender();
	}
}