package isep.ipp.pt.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.services.EventService;

@Component
public class RabbitReceiver {
	
	private static Log log = LogFactory.getLog(RabbitReceiver.class);
	
	@Autowired
	EventService service;
	
	@Value("${gu_command_queue.name}")
	private String guQueueName;
	
	@Bean
	Queue guQueue() {
		return new Queue(guQueueName, false);
	}
	
	@RabbitListener(queues = "${gu_command_queue.name}")
    public void receive(String in) {
		Gson g = new Gson();
		
		JsonObject json = g.fromJson(in, JsonObject.class);
		String type = json.get("eventType").getAsString();
		
		log.info("[X] Received " + type);
		
		double oldCondition, newCondition;
		int idUtilizador;
		
		log.info("JSON recebido: " + json);
		
		switch(type) {
			case "EMPRESTIMO_CLOSED_LATE":
				oldCondition = json.get("oldCondition").getAsDouble();
				newCondition = json.get("newCondition").getAsDouble();
				idUtilizador = json.get("idUtilizador").getAsInt();
				service.updateUserWithTimePenalty(idUtilizador, oldCondition, newCondition);
				break;
			case "EMPRESTIMO_CLOSED_ON_TIME":
				oldCondition = json.get("oldCondition").getAsDouble();
				newCondition = json.get("newCondition").getAsDouble();
				idUtilizador = json.get("idUtilizador").getAsInt();
				service.updateUserWithoutTimePenalty(idUtilizador, oldCondition, newCondition);
				break;
			default:
				log.info("Event Type not found " + type);
		}
    }
	
}
