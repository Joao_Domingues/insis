package isep.ipp.pt.domain.query;

public class Reputacao implements ValueObject {
	
	private double valor;
	
	public Reputacao() {
		this.valor = 2.5;
	}
	
	public double getValor() {
		return this.valor;
	}
	
	public void setValor(int valor) {
		this.valor = valor;
	}
	
	@Override
    public String toString() {
        return String.valueOf(valor);
    }
}
