package isep.ipp.pt.domain.query;

import java.io.Serializable;

public interface ValueObject extends Serializable {

	@Override
	String toString();

	@Override
	boolean equals(Object other);

	@Override
	int hashCode();

}
