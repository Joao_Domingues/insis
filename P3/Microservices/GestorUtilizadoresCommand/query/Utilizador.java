package isep.ipp.pt.domain.query;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Utilizador {

	@Id
	@GeneratedValue
	private int id;
	private String nome;
	private Estado estado;
	private Reputacao reputacao;
	private Date ultimaSuspensão;

	public Utilizador() {

	}

	public Utilizador(String nome) {
		this.nome = nome;
		this.estado = Estado.ATIVO;
		this.reputacao = new Reputacao();
		this.ultimaSuspensão = Date.valueOf(LocalDate.MIN);
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getEstado() {
		return estado.toString();
	}

	public Reputacao getReputacao() {
		return reputacao;
	}
	
	public Date getUltimaSuspensao() {
		return this.ultimaSuspensão;
	}

}
