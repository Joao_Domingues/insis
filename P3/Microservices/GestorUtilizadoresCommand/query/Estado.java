package isep.ipp.pt.domain.query;

public enum Estado implements ValueObject {
	ATIVO {
		@Override
		public String toString() {
			return "Ativo";
		}
	},
	INATIVO {
		@Override
		public String toString() {
			return "Inativo";
		}
	},
	SUSPENSO {
		@Override
		public String toString() {
			return "Suspenso";
		}
	}
}
