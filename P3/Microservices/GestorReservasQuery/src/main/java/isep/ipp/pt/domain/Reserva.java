package isep.ipp.pt.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Reserva {

	@Id
	private int id;
	private LocalDateTime inicio;
	private LocalDateTime fim;
	private int idUtilizador;
	private String tituloObra;
	private ReservaState state;
	
	public Reserva() {
		
	}
	
	public Reserva(LocalDateTime inicio, LocalDateTime fim, int idUtilizador, String tituloObra, ReservaState state) {
		this.inicio = inicio;
		this.fim = fim;
		this.idUtilizador = idUtilizador;
		this.tituloObra = tituloObra;
		this.state = state;
	}
	
	public int getId() {
		return this.id;
	}
	
	public LocalDateTime getInicio() {
		return this.inicio;
	}
	
	public LocalDateTime getFim() {
		return this.fim;
	}
	
	public int getIdUtilizador() {
		return this.idUtilizador;
	}
	
	
	public void setInicio(LocalDateTime inicio) {
		this.inicio = inicio;
	}
	
	public void setFim(LocalDateTime fim) {
		this.fim = fim;
	}
	
	public void setIdUtilizador(int idUtilizador) {
		this.idUtilizador = idUtilizador;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTituloObra() {
		return tituloObra;
	}

	public void setTituloObra(String tituloObra) {
		this.tituloObra = tituloObra;
	}

	public ReservaState getState() {
		return state;
	}

	public void setState(ReservaState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "\nid: " + this.id + 
				"\ninicio: " + this.inicio.toString() + 
				"\nfim: " + this.fim.toString() + 
				"\niduser: " + this.idUtilizador + 
				"\ntitulo: " + this.tituloObra +
				"\nstate: " + this.state;
	}
	
}
