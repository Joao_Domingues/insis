package isep.ipp.pt.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

import isep.ipp.pt.domain.Reserva;

@Service
public class RabbitSender {
	
	private static Log log = LogFactory.getLog(RabbitSender.class);
	
	@Autowired
	private RabbitTemplate template;

	@Value("${fanout.exchange}")
	private String fanoutExchange;

	public void sendFanout(Reserva res, String type) {
		
		log.info("Reserva: " + res);
		log.info("Type: " + type);
		
		JsonObject json = new JsonObject();
		json.addProperty("idRes", res.getId());
		json.addProperty("inicio", res.getInicio().toString());
		json.addProperty("fim", res.getFim().toString());
		json.addProperty("idUtilizador", res.getIdUtilizador());
		json.addProperty("tituloObra", res.getTituloObra());
		json.addProperty("state", res.getState().toString());
		json.addProperty("eventType", type);
		
		log.info("JsonObject: " + json);
		
		this.template.setExchange(fanoutExchange);
		this.template.convertAndSend(json.toString());
		log.info(" [x] Sent '" + type + "'");
	}
}
