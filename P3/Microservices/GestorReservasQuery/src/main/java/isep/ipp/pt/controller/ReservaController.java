package isep.ipp.pt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import isep.ipp.pt.domain.Reserva;
import isep.ipp.pt.messaging.RabbitSender;
import isep.ipp.pt.services.ReservaService;

@RestController
public class ReservaController {

	@Autowired
	ReservaService reservaService;
	
	@Autowired
	RabbitSender rabbitSender;

	@GetMapping("/reservas")
	private List<Reserva> getAllReservas() {
		return reservaService.getAllReservas();
	}

	@GetMapping("/reservas/{id}")
	private Reserva getReserva(@PathVariable("id") int id) {
		return reservaService.getReservaById(id);
	}

}