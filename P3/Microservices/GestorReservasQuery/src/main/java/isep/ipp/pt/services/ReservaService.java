package isep.ipp.pt.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import isep.ipp.pt.domain.Reserva;
import isep.ipp.pt.messaging.RabbitSender;
import isep.ipp.pt.repositories.ReservaRepository;

@Service
public class ReservaService {

	private static Log log = LogFactory.getLog(ReservaService.class);
	
	@Autowired
	ReservaRepository reservaRepository;

	@Autowired
	RabbitSender rabbitSender;
	
	public List<Reserva> getAllReservas() {
		List<Reserva> reservas = new ArrayList<Reserva>();
		reservaRepository.findAll().forEach(reserva -> reservas.add(reserva));
		return reservas;
	}

	public Reserva getReservaById(int id) {
		return reservaRepository.findById(id).get();
	}
	
	/**
	 * Received from GR_Command
	 * Creates a Reserva Pending, sends a message to GA
	 * @param r Reserva
	 */
	public void createReservaPending(Reserva r) {
		Reserva res = reservaRepository.save(r);
		rabbitSender.sendFanout(res, "RESERVA_CHECK_AUTH");
	}
	
	public void createReserva(Reserva r) {
		reservaRepository.save(r);
		log.info("End of Reserva Creation Use Case with Reserva: " + r);
	}

}
