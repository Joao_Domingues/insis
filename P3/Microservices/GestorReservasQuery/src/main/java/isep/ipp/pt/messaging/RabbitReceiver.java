package isep.ipp.pt.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.domain.*;
import isep.ipp.pt.services.ReservaService;

@Component
public class RabbitReceiver {
	
	private static Log log = LogFactory.getLog(RabbitReceiver.class);
	
	@Autowired
	ReservaService service;
	
	@Value("${gr_query_queue.name}")
	private String grQueueName;
	
	@Bean
	Queue grQueue() {
		return new Queue(grQueueName, false);
	}
	
	@RabbitListener(queues = "${gr_query_queue.name}")
    public void receive(String in) {
		
		Gson g = new Gson();
		
		JsonObject json = g.fromJson(in, JsonObject.class);

		JsonObject resData = g.fromJson(json.get("data"), JsonObject.class);
		Reserva reserva = g.fromJson(json.get("data"), Reserva.class);
		int realId = resData.get("idRes").getAsInt();
		reserva.setId(realId);
		
		log.info("Reserva received: " + reserva);

		String type = json.get("eventType").getAsString();
		log.info("[X] Received " + type);
		
		switch(type) {
			case "RESERVA_PENDING":
				service.createReservaPending(reserva);
				break;
			case "CREATE_RESERVA":
				service.createReserva(reserva);
				break;
			case "CANCELED_RESERVA":
				service.createReserva(reserva);
				break;
			case "END_RESERVA":
				service.createReserva(reserva);
				break;
			default:
				log.info("Event Type not found " + type);
		}
    }
	
}
