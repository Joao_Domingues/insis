package isep.ipp.pt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestorReservasApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestorReservasApplication.class, args);
	}

}
