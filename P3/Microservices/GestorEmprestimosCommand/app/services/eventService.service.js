const Emprestimo = require('../models/emprestimo.model.js');
const UserEvent = require('../models/userEvent.model.js');
const Send = require('../messaging/send.js');
const ObjectID = require('mongodb').ObjectID;

module.exports = {

  createEmprestimoPendente : function(emprestimo, controlExisteReserva) {
    console.log("Entrou no createEmprestimoPendente");
    console.log("New ID: "+ emprestimo.idEmp);
    emprestimo.idEmp = new ObjectID(emprestimo.idEmp)
    // Save Emprestimo in the database
    emprestimo.save()
    .then(data => {
        var evento = new UserEvent({
          emprestimo: data,
          timestamp: new Date(),
          eventType: 'RequestCreate'
        });
        evento.save()
        .then(data2 => {
            Send.sendFanout(data, "EMPRESTIMO_PENDING"); // to GE_Query
            if(controlExisteReserva){
              Send.sendFanout(data, "EMPRESTIMO_CHANGE_RESERVA_STATE"); // to GE_Query
            }
        }).catch(err2 => {
            console.log("[ERROR][emprestimoServices.services.js - createEmprestimoPendente] - Failed to save Event in database!");
        });
    }).catch(err => {
        console.log("[ERROR][emprestimoServices.services.js - createEmprestimoPendente] - Failed to save Emprestimo in database!");
    });
    //Send.sendFanout(emprestimo, "EMPRESTIMO_PENDING"); // to GE_Query
  },

  createEmprestimoDefinitivo : function(response, estado) {
    console.log("Entrou no createEmprestimoDefinitivo: " + JSON.stringify(response));

    var emprestimo = new Emprestimo({
        idEmp: response._id,
        inicio: response.inicio,
        fim: response.fim,
        idUtilizador: response.idUtilizador,
        tituloObra: response.tituloObra,
        estado: estado
    });

    var eventType = 'Create';

    if (estado==='Cancelado') eventType = 'Canceled';
    var evento = new UserEvent({
      emprestimo: emprestimo,
      timestamp: new Date(),
      eventType: eventType
    });

    console.log("Event created: " + evento);
    console.log("Emprestimo updated: " + emprestimo);

    // Save Emprestimo in the database
    emprestimo.save()
    .then(data => {
        var evento = new UserEvent({
          emprestimo: data,
          timestamp: new Date(),
          eventType: eventType
        });
        evento.save()
        .then(data2 => {
            Send.sendFanout(data, "EMPRESTIMO_CREATED"); // to GE_Query
        }).catch(err2 => {
            console.log("[ERROR][emprestimoServices.services.js - createEmprestimoDefinitivo] - Failed to save Event in database! " + err.message);
        });
    }).catch(err => {
        console.log("[ERROR][emprestimoServices.services.js - createEmprestimoDefinitivo] - Failed to save Emprestimo in database! " + err.message);
    });

    // Send.sendFanout(emprestimo, "EMPRESTIMO_CREATED"); // to GE_Query
  },

  closeEmprestimo : function(emprestimo, oldCondition, newCondition) {
    console.log("[INFO][eventServices.service.js - closeEmprestimo] - Emprestimo para ser fechado: " + JSON.stringify(emprestimo));

    // Save Emprestimo in the database
    emprestimo.save()
    .then(data => {
        var evento = new UserEvent({
          emprestimo: data,
          timestamp: new Date(),
          eventType: 'Close'
        });
        // isto está aqui porque eu pensava que dava para adicionar campos livremente em js (e.g.: emprestimo.oldCondition = ...)
        var emprestimoJSON = JSON.stringify(emprestimo);
        emprestimoJSON = emprestimoJSON.substring(0, emprestimoJSON.length-1) + ",\"oldCondition\":\"" + oldCondition + "\"}";
        emprestimoJSON = emprestimoJSON.substring(0, emprestimoJSON.length-1) + ",\"newCondition\":\"" + newCondition + "\"}";
        var finalEmprestimo = JSON.parse(emprestimoJSON);
        evento.save()
        .then(data2 => {
            console.log("Empréstimo updated: " + JSON.stringify(data));
            Send.sendFanout(finalEmprestimo, "CLOSE_EMPRESTIMO"); // to GE_Query
        }).catch(err2 => {
            console.log("[ERROR][emprestimoServices.services.js - closeEmprestimo] - Failed to save Event in database! " + err.message);
        });
    }).catch(err => {
      console.log("[INFO][eventServices.service.js - closeEmprestimo] - Erro a atualizar o Emprestimo. " + err.message);
    });
  }


}
