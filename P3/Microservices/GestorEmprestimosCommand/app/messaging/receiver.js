#!/usr/bin/env node
const Service = require('../services/eventService.service.js');
var amqp = require('amqplib/callback_api');

const ge_queue = "GE_Command_Queue";

const exchange = "GE_Command_exchange";

module.exports = {
  executeRabbitReceiver : function() {
    amqp.connect('amqp://192.168.99.100:5672', function(error0, connection) {
      if (error0) {
          throw error0;
      }
      connection.createChannel(function(error1, channel) {
          if (error1) {
              throw error1;
          }

          channel.assertQueue(ge_queue, {
              durable: false
          });

          console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", ge_queue);

          channel.consume(ge_queue, function(msg) {
              var response = JSON.parse(msg.content.toString());
              console.log("[INFO][receiver.js] - Received event of type: " + response.eventType);
              switch(response.eventType) {
                case "HAS_AUTHORIZATION_EMPRESTIMO":
                  Service.createEmprestimoDefinitivo(response, 'Ativo');
                  break;
                case "ERROR_AUTHORIZATION_EMPRESTIMO":
                  Service.createEmprestimoDefinitivo(response, 'Cancelado');
                  break;
                default:
                  console.log("[WARNING][receiver.js] - No such event type found.");
              }
          }, {
              noAck: true
          });
      });
    });
  }
}
