const Emprestimo = require('../models/emprestimo.model.js');
const UserEvent = require('../models/userEvent.model.js')
const Send = require('../messaging/send.js');
const Service = require('../services/eventService.service.js');
const http = require('request');

// Create and Save a new Emprestimo
exports.create = (req, res) => {
    // Validate request
    if(!req.body.inicio || !req.body.fim || !req.body.idUtilizador || !req.body.tituloObra) {
        return res.status(400).send({
            message: "Emprestimo is not valid"
        });
    }

    // Create a Emprestimo
    const emprestimo = new Emprestimo({
        idEmp: req.body.idEmp,
        inicio: req.body.inicio,
        fim: req.body.fim,
        idUtilizador: req.body.idUtilizador,
        tituloObra: req.body.tituloObra,
        estado: 'Pendente'
    });

    getUtilizador(emprestimo, function(info) {
        var controlEstadoUtilizador = false;
        info = JSON.parse(info);
        if(info.estado==="INATIVO" || info.estado==="SUSPENSO" || info.status===500){
            controlEstadoUtilizador = true;
        }
        console.log("valor estado: " + controlEstadoUtilizador)
        if(!controlEstadoUtilizador){
            intersectReserva(function(info) {
                console.log(info);
                var split = JSON.parse(info);
                var controlExisteReserva = false;
                var controlIntersectReserva = false;
                var controlIntersectEmprestimo = false;
                split.forEach(reserva => {
                    // verifica que o emprestimo provem duma reserva existente
                    if(reserva.tituloObra===emprestimo.tituloObra && reserva.idUtilizador===emprestimo.idUtilizador && (reserva.state === "PENDING" ||reserva.state === "UP_TO_DATE") ){
                        console.log("Entrei aqui")
                        var stringInicio = reserva.inicio;
                        var stringFim = reserva.fim;
                        var splitInicio = stringInicio.split("-");
                        var splitFim = stringFim.split("-");
                        var dataInicio = new Date(splitInicio[0] + "-" + splitInicio[1] + "-" + splitInicio[2].split("T", 1));
                        var dataFim = new Date(splitFim[0] + "-" + splitFim[1] + "-" + splitFim[2].split("T", 1))
                        var empInicio = emprestimo.inicio
                        empInicio.setHours(10,0,0,0)
                        var empFim = emprestimo.fim
                        empFim.setHours(10,0,0,0)
                        dataInicio.setHours(10,0,0,0)
                        emprestimo.fim.setHours(10,0,0,0)
                        dataFim.setHours(10,0,0,0)
                        if(dataInicio.getTime() === empInicio.getTime() && dataFim.getTime() === empFim.getTime()){
                            controlExisteReserva = true;
                        }
                    }

                    // verifica que a obra nao possui reservas para o período indicado
                    if(reserva.tituloObra===emprestimo.tituloObra && !controlExisteReserva && (reserva.state === "PENDING" || reserva.state === "UP_TO_DATE")){
                        var stringInicio = reserva.inicio;
                        var stringFim = reserva.fim;
                        var splitInicio = stringInicio.split("-");
                        var splitFim = stringFim.split("-");
                        var dataInicio = new Date(splitInicio[0] + "-" + splitInicio[1] + "-" + splitInicio[2].split("T", 1));
                        var dataFim = new Date(splitFim[0] + "-" + splitFim[1] + "-" + splitFim[2].split("T", 1))

                        if(dataInicio<emprestimo.fim && dataInicio>emprestimo.inicio) {
                            controlIntersectReserva = true;
                        } else if (emprestimo.inicio < dataFim && emprestimo.inicio > dataInicio) {
                            controlIntersectReserva = true;
                        } else if (emprestimo.inicio.getTime() === dataInicio.getTime() || emprestimo.fim.getTime() === dataFim.getTime()){
                            controlIntersectReserva = true;
                        };
                    };
                });
                    console.log("CONTROL EXISTE RESERVA: " + controlExisteReserva)
                    // verifica que nao existe interseçao de emprestimos para a obra no período indicado
                    if(!controlExisteReserva && !controlIntersectReserva){
                        getAllEvents(function(info) {
                        //var split2 = JSON.parse(info);
                        var events = [];
                        info.forEach(evento => {
                            if(evento.emprestimo.tituloObra === emprestimo.tituloObra && evento.emprestimo.idEmp != '0' && evento.eventType != "Canceled"){
                            events.push(evento);
                            }
                        })

                        // key -> idEmprestimo, value -> userEvent
                        var map = new Map();
                        events.forEach(ev => {
                            if(map.has(ev.emprestimo.idEmp)){
                                if(map.get(ev.emprestimo.idEmp).timestamp < ev.timestamp){
                                    map.set(ev.emprestimo.idEmp, ev)
                                }
                            } else {
                                map.set(ev.emprestimo.idEmp, ev)
                            }
                        })


                        map.forEach(ev => {
                            console.log("Current event: " + ev)
                            if(ev.emprestimo.inicio<=emprestimo.fim && ev.emprestimo.inicio>=emprestimo.inicio) {
                                controlIntersectEmprestimo = true;
                            } else if (emprestimo.inicio <= ev.emprestimo.fim && emprestimo.inicio >= ev.emprestimo.inicio) {
                                controlIntersectEmprestimo = true;
                            } else if (emprestimo.inicio.getTime() === ev.emprestimo.inicio.getTime() || emprestimo.fim.getTime() === ev.emprestimo.fim.getTime() ){
                                controlIntersectEmprestimo = true;
                            };
                        })

                        if(controlIntersectEmprestimo){
                            return res.status(200).send({
                                message: false
                            });
                        } else {
                          if(emprestimo.idEmp == '0') {
                            console.log("Novo emprestimo");
                            getHexID((info.length+1).toString(), function(newId){
                                emprestimo.idEmp = newId;
                                Service.createEmprestimoPendente(emprestimo, controlExisteReserva);
                                return res.status(200).send({
                                    message: emprestimo
                                });
                            });
                          } else {
                            console.log("Emprestimo ja existente");
                            Service.createEmprestimoPendente(emprestimo, controlExisteReserva);
                            return res.status(200).send({
                                message: emprestimo
                            });
                          }
                        }


                        });

                    } else if(controlExisteReserva){
                        if(emprestimo.idEmp == '0') {
                            console.log("Novo emprestimo");
                            getHexID((info.length+1).toString(), function(newId){
                                emprestimo.idEmp = newId;
                                Service.createEmprestimoPendente(emprestimo, controlExisteReserva);
                                return res.status(200).send({
                                    message: emprestimo
                                });
                            });
                          } else {
                            console.log("Emprestimo ja existente");
                            Service.createEmprestimoPendente(emprestimo, controlExisteReserva);
                            return res.status(200).send({
                                message: emprestimo
                            });
                          }
                    } else {
                        return res.status(200).send({
                            message: false
                        });
                    }
                });
            } else {
                return res.status(200).send({
                    message: false
                });
            }
  });
}

function getHexID(id, callback){
    var size = 24 - id.length;
    var newId = '';
    for(i=0; i< size; i++){
        newId += '0';
    }
    newId += id;
    callback(newId);
}

function getUtilizador(emprestimo, callback){
    var id = emprestimo.idUtilizador;
    http('http://localhost:8080/utilizadores/' + id,function (error, response, body) {
       //console.error('error:', error); // Print the error if one occurred
       //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
       //console.log('body:', body); // Print the HTML for the Google homepage.
       callback(body, response.statusCode);
   });
}
function intersectReserva(callback){
   http('http://localhost:8082/reservas',function (error, response, body) {
       //console.error('error:', error); // Print the error if one occurred
       //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
       //console.log('body:', body); // Print the HTML for the Google homepage.
       callback(body);
   });
};

function getAllEvents(callback){
  UserEvent.find()
  .then(events => {
    callback(events);
  })
}

// Delete a emprestimo with the specified id in the request
exports.delete = (req, res) => {
  const emprestimo = new Emprestimo({
      idEmp: req.params.id, // the id received is the one refering to the query database
      inicio: req.body.inicio,
      fim: req.body.fim,
      idUtilizador: req.body.idUtilizador,
      tituloObra: req.body.tituloObra,
      estado: 'Fechado'
  });

  Service.closeEmprestimo(emprestimo, req.body.oldCondition, req.body.newCondition);

  return res.status(200).send({
      message: emprestimo
  });
};
