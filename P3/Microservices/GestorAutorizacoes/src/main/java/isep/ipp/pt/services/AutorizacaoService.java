package isep.ipp.pt.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.messaging.RabbitSender;

@Service
public class AutorizacaoService {
	
	private static Log log = LogFactory.getLog(AutorizacaoService.class);
	
	@Autowired
	RabbitSender sender;
	
	/**
	 * Comes from GR_Query/GE_Query
	 * Checks if the User that requested a Reserva/Emprestimo has authorization to do so
	 * If possible, sends a message to either GR_Command or GE_Command
	 */
	public void verificaAutorizacao(JsonObject json, String nextEventType, String errorEventType) throws IOException {
		

		
		if(utilizadorHasAuthorization(json.get("idUtilizador").getAsInt())) {
			json.addProperty("eventType", nextEventType);
			
			log.info("json: " + json);
			sender.sendFanout(json, nextEventType);
		} else {
			json.addProperty("eventType", errorEventType);
			
			log.info("json: " + json);
			sender.sendFanout(json, errorEventType);
		}
	}
	
	/**
	 * Checks if the user with the received ID can perform an action
	 */
	private boolean utilizadorHasAuthorization(Integer idUtilizador) throws IOException {
		Double reputacaoUtilizador = getReputacaoUtilizador(idUtilizador);
		System.out.println("Reputacao Utilizador: " + reputacaoUtilizador);
		Double estadoObra = getEstadoObra();
		
		log.info("Estado Obra Gerado: " + estadoObra);
		
		return ((estadoObra==3 && reputacaoUtilizador>3) || (estadoObra<3 && reputacaoUtilizador > 4) || (estadoObra >3));
	}
	
	private Double getReputacaoUtilizador(Integer idUtilizador) throws IOException {
		URL utilizadorURL = new URL("http://localhost:8080/utilizadores/" + idUtilizador + "/reputacao");
		
		HttpURLConnection utilizadorCon = (HttpURLConnection) utilizadorURL.openConnection();
		utilizadorCon.setRequestMethod("GET");
		
		StringBuffer utilizadorContent = new StringBuffer();
		BufferedReader in = new BufferedReader(
				new InputStreamReader(utilizadorCon.getInputStream()));
				String inputLine;
				
				while ((inputLine = in.readLine()) != null) {
					utilizadorContent.append(inputLine);
				}
		in.close();
		
		return Double.valueOf(utilizadorContent.toString());
	}
	
	/*
	private Double getEstadoObra(String tituloObra) throws IOException {
		URL obraURL = new URL("http://localhost:8280/polos/allPolos/titulo?t=" + "vestibulum sit amet");
		
		HttpURLConnection obraCon = (HttpURLConnection) obraURL.openConnection();
		obraCon.setRequestMethod("GET");
		
		StringBuffer obraContent = new StringBuffer();
		BufferedReader in = new BufferedReader(
				new InputStreamReader(obraCon.getInputStream()));
				String inputLine;
				
				while ((inputLine = in.readLine()) != null) {
					obraContent.append(inputLine);
				}
		in.close();
		
		return 0.0;
		
	}
	*/
	
	private Double getEstadoObra() {
		int rangeMin = 1;
		int rangeMax = 6;
		Random r = new Random();
		return rangeMin + (rangeMax - rangeMin) * r.nextDouble();
	}
}
