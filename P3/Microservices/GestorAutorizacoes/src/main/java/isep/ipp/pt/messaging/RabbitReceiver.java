package isep.ipp.pt.messaging;

import java.io.IOException;
import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import isep.ipp.pt.services.AutorizacaoService;

@Component
public class RabbitReceiver {
	
	private static Log log = LogFactory.getLog(RabbitReceiver.class);
	
	@Autowired
	AutorizacaoService service;
	
	@Value("${ga_queue.name}")
	private String gaQueueName;
	
	@Bean
	Queue gaQueue() {
		return new Queue(gaQueueName, false);
	}
	
	// These should be moved to the service
	private final static String HAS_AUTHORIZATION_RESERVA = "HAS_AUTHORIZATION_RESERVA";
	private final static String ERROR_AUTHORIZATION_RESERVA = "ERROR_AUTHORIZATION_RESERVA";
	private final static String HAS_AUTHORIZATION_EMPRESTIMO = "HAS_AUTHORIZATION_EMPRESTIMO";
	private final static String ERROR_AUTHORIZATION_EMPRESTIMO = "ERROR_AUTHORIZATION_EMPRESTIMO";
	
	@RabbitListener(queues = "${ga_queue.name}")
    public void receive(String in) throws IOException {
		Gson g = new Gson();
		
		JsonObject json = g.fromJson(in, JsonObject.class);
		String type = json.get("eventType").getAsString();
		
		log.info("[X] Received " + type);
		
		switch(type) {
			case "RESERVA_CHECK_AUTH": // proveniente do gr_query
				service.verificaAutorizacao(json, HAS_AUTHORIZATION_RESERVA, ERROR_AUTHORIZATION_RESERVA);
				break;
			case "EMPRESTIMO_CHECK_AUTH": // proveniente do ge_query
				service.verificaAutorizacao(json, HAS_AUTHORIZATION_EMPRESTIMO, ERROR_AUTHORIZATION_EMPRESTIMO);
				break;
			default:
				log.info("Event Type not found " + type);
		}
    }
	
}
