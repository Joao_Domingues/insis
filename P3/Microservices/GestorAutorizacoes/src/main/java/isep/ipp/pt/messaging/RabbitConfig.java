package isep.ipp.pt.messaging;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class RabbitConfig {

	@Value("${fanout.exchange}")
	private String fanoutExchange;

	@Value("${gr_command_queue.name}")
	private String grCommandQueueName;
	
	@Value("${ge_command_queue.name}")
	private String geCommandQueueName;

	@Bean
	Queue grCommandQueue() {
		return new Queue(grCommandQueueName, false);
	}
	
	@Bean
	Queue geCommandQueue() {
		return new Queue(geCommandQueueName, false);
	}

	@Bean
	FanoutExchange exchange() {
		return new FanoutExchange(fanoutExchange);
	}

    @Bean
    public Binding binding1(FanoutExchange fanout) {
        return BindingBuilder.bind(grCommandQueue()).to(fanout);
    }

    @Bean
    public Binding binding2(FanoutExchange fanout) {
        return BindingBuilder.bind(geCommandQueue()).to(fanout);
    }
    
	@Profile("receiver")
	@Bean
	public RabbitReceiver receiver() {
		return new RabbitReceiver();
	}

	@Profile("sender")
	@Bean
	public RabbitSender sender() {
		return new RabbitSender();
	}
}