package isep.ipp.pt.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitSender {
	
	private static Log log = LogFactory.getLog(RabbitSender.class);
	
	@Autowired
	private RabbitTemplate template;

	@Value("${fanout.exchange}")
	private String fanoutExchange;

	public void sendFanout(Object obj, String type) {
		
		this.template.setExchange(fanoutExchange);
		this.template.convertAndSend(obj.toString());
		log.info(" [x] Sent '" + type + "'");
	}
}
