package bpmn.customType.type;

import org.activiti.engine.form.AbstractFormType;

public class ObrasFornecedoresFormType extends AbstractFormType {

	public static final String TYPE_NAME = "obrasFornecedores";
	 
    public String getName() {
        return TYPE_NAME;
    }
 
    @Override
    public Object convertFormValueToModelValue(String propertyValue) {
        Integer table = Integer.valueOf(propertyValue);
        return table;
    }
 
    @Override
    public String convertModelValueToFormValue(Object modelValue) {
        if    (modelValue    == null) {
            return null;
        }
        return modelValue.toString();
    }

}
