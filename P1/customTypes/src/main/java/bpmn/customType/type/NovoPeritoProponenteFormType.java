package bpmn.customType.type;

import org.activiti.engine.form.AbstractFormType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NovoPeritoProponenteFormType extends AbstractFormType {

	public static final String TYPE_NAME = "novoPeritoProponente";
 
    public String getName() {
        return TYPE_NAME;
    }
 
    @Override
    public Object convertFormValueToModelValue(String propertyValue) {

        Integer table = Integer.valueOf(propertyValue);
        return table;
    }
 
    @Override
    public String convertModelValueToFormValue(Object modelValue) {

        if    (modelValue    == null) {
            return null;
        }
        return modelValue.toString();
    }
}

