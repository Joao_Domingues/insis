package bpmn.customType.utils.config;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import bpmn.customType.utils.ContadorPassagensAprovaPAD;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger; 

public class TimerConfiguration implements JavaDelegate {

	private static Log log = LogFactory.getLog(TimerConfiguration.class);
    HashMap<String, String> properties = new HashMap<String, String>();          

    @SuppressWarnings("rawtypes")
    public void execute(DelegateExecution execution) throws Exception {
         
    	  log.info("Config File Service Task");
          ConfigFile file = new ConfigFile();           

          this.properties = file.getProperties();                                                 

          Iterator entries = this.properties.entrySet().iterator();
          while (entries.hasNext()) {
              HashMap.Entry entry = (HashMap.Entry) entries.next();
              String key = entry.getKey().toString();
              String value = entry.getValue().toString();     
              
              log.info("key: " + key + " value: " + value);

              execution.setVariable(key, value);
          } 
     

    }

}
