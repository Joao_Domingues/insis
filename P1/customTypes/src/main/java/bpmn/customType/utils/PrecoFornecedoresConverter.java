package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PrecoFornecedoresConverter implements JavaDelegate {

	private static Log log = LogFactory.getLog(PrecoFornecedoresConverter.class);
	
	/**
	 * Recolhe os pre�os dos fornecedores, verifica o mais barato e agrega a informa��o
	 */
	public void execute(DelegateExecution execution) throws Exception {

		log.info("Chegou ao PrecoFornecedoresConverter");
		
		String infoFornecedores = execution.getVariable("infoObrasFornecedores").toString();
		
		log.info("infoFornecedores: " + infoFornecedores);
		
		if(infoFornecedores.length() < 5) {
			log.info("entrou");
			execution.setVariable("precoMaisBaixo", "0");
			execution.setVariable("fornecedor", "Nenhum fornecedor tem a obra para venda");
			return;
		}
		
		List<String> fornecedores = specialSplit(infoFornecedores);
		
		double preco = Double.MAX_VALUE;
		String fornecedor = "";
		
		for(String f : fornecedores) {
			String[] parametros = f.split(",");
			for(String p : parametros) {
				String[] subParametros = p.split("=");
				if(subParametros[0].contains("preco")) {
					double curr = Double.parseDouble(subParametros[1]);
					if(curr < preco) {
						preco = curr;
						for(String p2 : parametros) {
							String[] subParametros2 = p2.split("=");
							log.info("parametro: " + p2);
							if(subParametros2[0].contains("fornecedor")) {
								log.info("entrou");
								fornecedor = subParametros2[1];
							}
						}
					}
				}
			}
		}
		
		log.info("preco: " + String.valueOf(preco));
		log.info("fornecedor: " + fornecedor);
		
		execution.setVariable("precoMaisBaixo", String.valueOf(preco) + "�");
		execution.setVariable("fornecedor", fornecedor);
		
	}
	
	/**
	 * Separa os fornecedores e mete os que obtiveram resposta numa lista, retornando-a
	 * @param toSplit
	 * @return
	 */
	private List<String> specialSplit(String toSplit) {
		List<String> split = new ArrayList<String>();
		int currentIndex = 0, level = 0, lastIndex = 0;
		String section;
		
		log.info("specialSplit string de entrada: " + toSplit);

		while(currentIndex < toSplit.length()) {
			if(toSplit.charAt(currentIndex) == '{') {
				if(level == 0) lastIndex = currentIndex+1;
				level++;
			}
			
			if(toSplit.charAt(currentIndex) == '}') {
				level--;
				if(level == 0) {
					section = toSplit.substring(lastIndex, currentIndex);
					if(!section.contains("preco=-1")) {
						log.info("specialSplit section found: " + section);
						split.add(section);
					}	
				}
			}
			currentIndex++;
		}
		
		log.info("specialSplit number of sections found: " + split.size() + " result: " + split);
		
		return split;
	}

}
