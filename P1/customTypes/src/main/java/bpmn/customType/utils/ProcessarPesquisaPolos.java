package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ProcessarPesquisaPolos implements JavaDelegate {

	private static Log log = LogFactory.getLog(ProcessarPesquisaPolos.class);
	
	public void execute(DelegateExecution execution) throws Exception {
		
		log.info(" -- ProcessarObrasPolos -- ");
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, ArrayList<String>> map = mapper.readValue(execution.getVariable("obrasPolos").toString(), Map.class);
		
		String aux = map.get("value").toString();
		// execution.setVariable("obrasPolos", aux);
		
		boolean existemObras = aux.contains("obraSelecionada");
		execution.setVariable("existemObrasPolos", existemObras);
		
		log.info("Existem obras nos polos? " + existemObras);
		
		// se n�o existirem obras, sai
		if(!existemObras) {
			execution.setVariable("isbn", "");
			execution.setVariable("infoObrasPolos", "");
			return;
		}
		
		// vai buscar o ISBN
		String isbn = aux.split("ISBN=")[1];
		isbn = isbn.split(",")[0];
		log.info("isbn: " + isbn);
		execution.setVariable("isbn", isbn);
		
		// lista contendo apenas as obras selecionadas (remove obras n�o encontradas)
		List<String> obrasSelecionadas = specialSplit(aux);
		
		String stringFinal = "";
		
		for(String obra : obrasSelecionadas) {
			log.info("obra: " + obra);
			stringFinal += "{";
			
			String[] parametros = specialReplace(specialSplit(obra).get(0)).split(";");
			log.info("parametros:" + parametros);
			
			for(String parametro : parametros) {
				log.info("parametro input: " + parametro);
				String[] auxArray = parametro.split("=");
				String type = auxArray[0];
				log.info("parametro type: " + type);

				if(type.contains("Preco") || type.contains("Polo") || type.contains("QtdExemplares")) {
					stringFinal += type + ":" + auxArray[1] + ",";
				} else if(type.contains("Encadernacao") || type.contains("AreaConhecimento")) {
					stringFinal += type + ":" + getSubTypeValue(parametro) + ",";
				} else if(type.contains("EstadosID")) {
					stringFinal += type + ":" + generateStatus(parametro) + ",";
				}
				
				log.info("string final depois de parametro output: " + stringFinal);
			}
			
			stringFinal = stringFinal.substring(0, stringFinal.length()-1);
			stringFinal += "};";
			log.info("string final depois de obra output: " + stringFinal);
		}
		
		stringFinal = stringFinal.substring(0, stringFinal.length()-1);
		
		log.info("resultado final: " + stringFinal);
		
		execution.setVariable("infoObrasPolos", stringFinal);
	}
	
	/**
	 * Separa os polos e mete os que obtiveram resposta numa lista, retornando-a
	 * @param toSplit
	 * @return
	 */
	private List<String> specialSplit(String toSplit) {
		List<String> split = new ArrayList<String>();
		int currentIndex = 0, level = 0, lastIndex = 0;
		String section;
		
		log.info("specialSplit string de entrada: " + toSplit);

		while(currentIndex < toSplit.length()) {
			if(toSplit.charAt(currentIndex) == '{') {
				if(level == 0) lastIndex = currentIndex+1;
				level++;
			}
			
			if(toSplit.charAt(currentIndex) == '}') {
				level--;
				if(level == 0) {
					section = toSplit.substring(lastIndex, currentIndex);
					if(!section.contains("error=Obra not found")) {
						log.info("specialSplit section found: " + section);
						split.add(section);
					}	
				}
			}
			currentIndex++;
		}
		
		log.info("specialSplit number of sections found: " + split.size() + " result: " + split);
		
		return split;
	}
	
	private String specialReplace(String toReplace) {
		int currentIndex = 0, level = 0;
		
		while(currentIndex < toReplace.length()) {
			if(toReplace.charAt(currentIndex) == '{' || toReplace.charAt(currentIndex) == '[') level++;
			if(toReplace.charAt(currentIndex) == '}' || toReplace.charAt(currentIndex) == ']') level--;
			if(toReplace.charAt(currentIndex) == ',' && level == 0) {
				toReplace = changeCharInPosition(currentIndex, ';', toReplace);
			}
			
			currentIndex++;
		}
		
		log.info("specialReplace result: " + toReplace);
		
		return toReplace;
	}
	
	private String changeCharInPosition(int position, char ch, String str){
	    char[] charArray = str.toCharArray();
	    charArray[position] = ch;
	    return new String(charArray);
	}
	
	private String getSubTypeValue(String s) {
		List<String> ls = specialSplit(s);
		log.info("getSubTypeValue ls: " + ls);
		String toReturn = "";
		
		String[] aux1 = ls.get(0).split(",");
		
		for(String ss : aux1) {
			log.info("getSubTypeValue ss: " + ss);
			String[] aux2 = ss.split("=");
			log.info("getSubTypeValue subType: " + aux2[0]);
			if(!aux2[0].contains("id")) toReturn = aux2[1];
		}
		
		return toReturn;
	}
	
	private String generateStatus(String input) {
		// DIVIDIR POR &
		List<String> estados = specialSplit(input);
		String toReturn = "";
		
		log.info("Estados : " + estados);
		
		for(String estado : estados) {
			log.info("Estado : " + estado);
			String[] parametros = estado.split(",");
			for(String parametro : parametros) {
				log.info("parametro : " + parametro);
				String[] subParametro = parametro.split("=");
				if(subParametro[0].contains("denominacao")) toReturn += subParametro[1] + " & ";
			}
		}
		
		toReturn = toReturn.substring(0, toReturn.length()-3);
		log.info("Estados resultado: " + toReturn);
		return toReturn;
	}

}
