package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class PeritoConverter implements JavaDelegate {

	/**
	 * Converte os emails inseridos pelo Proponente e Bibliotecário-Mor
	 * em Peritos, para serem utilizados no multi-instance task
	 */
	public void execute(DelegateExecution execution) throws Exception {

		String peritosProponente = (String) execution.getVariable("peritosProponente");
		String[] arrayProponente = peritosProponente.split(";");
		
		String peritosBM = (String) execution.getVariable("peritosBM");
		String[] arrayBM = peritosBM.split(";");
		
		List<String> peritosList = new ArrayList<String>();

		for (String s : arrayProponente) {
			String[] a = s.split("@");
			peritosList.add(a[0]);
		}
		
		for (String s : arrayBM) {
			String[] a = s.split("@");
			peritosList.add(a[0]);
		}
		
		//peritosList.add("admin");

		execution.setVariable("peritosList", peritosList);
	}

}