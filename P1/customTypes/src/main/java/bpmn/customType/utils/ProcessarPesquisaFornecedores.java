package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ProcessarPesquisaFornecedores implements JavaDelegate {

	private static Log log = LogFactory.getLog(ProcessarPesquisaFornecedores.class);
	
	public void execute(DelegateExecution execution) throws Exception {

		log.info("\n\n -- ProcessarObrasFornecedores -- ");
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, ArrayList<String>> map = mapper.readValue(execution.getVariable("obrasFornecedores").toString(), Map.class);

		log.info("map: " + map);
		
		String aux = map.get("value").toString();
		
		log.info("value: " + aux);
		
		boolean existemObras = aux.contains("Obra");
		execution.setVariable("existemObrasFornecedores", existemObras);
		
		log.info("Existem obras nos fornecedores? " + existemObras);
		
		// se n�o existirem obras, sai
		if(!existemObras) {
			execution.setVariable("infoObrasFornecedores", "");
			return;
		}
		
		// lista contendo apenas as obras selecionadas (remove obras n�o encontradas)
		List<String> obrasSelecionadas = specialSplit(aux);
		
		log.info("List obrasSelecionadas: " + obrasSelecionadas);
		
		String stringFinal = "";
		
		for(String obra : obrasSelecionadas) {
			log.info("obra: " + obra);
			
			String[] parametros = specialSplit(obra).get(0).split(",");
			log.info("parametros:" + parametros);
			stringFinal += "{";
			for(String parametro : parametros) {
				stringFinal += parametro + ",";
			}
			stringFinal = stringFinal.substring(0, stringFinal.length()-1);
			stringFinal += "};";
		}
		
		stringFinal = stringFinal.substring(0, stringFinal.length()-1);
		
		log.info("resultado final: " + stringFinal);
		
		execution.setVariable("infoObrasFornecedores", stringFinal);
	}

	/**
	 * Separa os fornecedores e mete os que obtiveram resposta numa lista, retornando-a
	 * @param toSplit
	 * @return
	 */
	private List<String> specialSplit(String toSplit) {
		List<String> split = new ArrayList<String>();
		int currentIndex = 0, level = 0, lastIndex = 0;
		String section;
		
		log.info("specialSplit string de entrada: " + toSplit);

		while(currentIndex < toSplit.length()) {
			if(toSplit.charAt(currentIndex) == '{') {
				if(level == 0) lastIndex = currentIndex+1;
				level++;
			}
			
			if(toSplit.charAt(currentIndex) == '}') {
				level--;
				if(level == 0) {
					section = toSplit.substring(lastIndex, currentIndex);
					if(!section.contains("preco=-1")) {
						log.info("specialSplit section found: " + section);
						split.add(section);
					}	
				}
			}
			currentIndex++;
		}
		
		log.info("specialSplit number of sections found: " + split.size() + " result: " + split);
		
		return split;
	}
	
}
