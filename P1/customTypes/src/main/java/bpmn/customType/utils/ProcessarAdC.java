package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ProcessarAdC implements JavaDelegate {

	private static Log log = LogFactory.getLog(ProcessarPolos.class);
	
	/**
	 * Processa o JSON recebido da /biblioteca/AdC, convertendo-o para uma lista de strings
	 */
	public void execute(DelegateExecution execution) throws Exception {
		
		log.info(" -- ProcessarAdC -- ");
		
		String aux = execution.getVariable("areas").toString();
		
		// remove []
		aux = aux.substring(1, aux.length()-1);
		
		// split by ,
		String[] areasArray = aux.split(",");
		
		List<String> areasList = new ArrayList<String>();
		for(String p : areasArray) {
			if(p.contains("}")) {
				// remove }
				p = p.substring(0, p.length()-1);
				// split by :, get second string
				p = p.split(":")[1];
				// remove ""
				p = p.substring(1, p.length()-1);
				areasList.add(p);
			}
		}
		
		log.info("Lista de areas: " + areasList);
		
		execution.setVariable("areas", areasList);

	}
}
