package bpmn.customType.utils;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FornecedorInvalido implements JavaDelegate {

	private static Log log = LogFactory.getLog(FornecedorInvalido.class);
	
	private static int NUMERO_FORNECEDORES = 3;
	
	public void execute(DelegateExecution execution) throws Exception {
		/*
		log.info("FornecedorInvalido");
		
		String falha = (String) execution.getVariable("REST_INVOKE_ERROR_MESSAGE"); // aqui temos a mensagem de erro, que cont�m o id e nome da task que falha
	
		String idService = "Fornecedor";
		String nomeVar = "precoFornecedor";
		
		log.info(execution.getVariable("REST_INVOKE_ERROR_MESSAGE"));
		
		for (int i = 0; i < NUMERO_FORNECEDORES; i++) {
			if(falha.contains(idService + String.valueOf(i))) {
				log.info(idService + String.valueOf(i) + " falhou no seu Rest Request!!");
				execution.setVariable(nomeVar + String.valueOf(i), "-1");
			}
		}
		*/
		
		String falha = (String) execution.getVariable("numFornecedor");

		log.info("fornecedor" + falha + " erro... valor recolhido: " + execution.getVariable("precoFornecedor"));
		
		execution.setVariable("precoFornecedor", "-1");
			
	}

}
