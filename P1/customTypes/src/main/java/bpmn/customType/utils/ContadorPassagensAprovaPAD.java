package bpmn.customType.utils;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ContadorPassagensAprovaPAD implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(ContadorPassagensAprovaPAD.class);

	/**
	 * Conta o n�mero de vezes que o Administrador j� rejeitou a Proposta
	 */
	public void execute(DelegateExecution execution) throws Exception {
		
		log.info("ContarPareceres iniciado");
		
		if (execution.getVariable("contador") == null) {
			execution.setVariable("contador", 1);
		} else {
			int count = (Integer) execution.getVariable("contador");
			count++;
			execution.setVariable("contador", count);
		}
	}
}
