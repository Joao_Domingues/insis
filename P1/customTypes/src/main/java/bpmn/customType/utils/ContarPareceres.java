package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import bpmn.customType.listeners.ParecerListener;

public class ContarPareceres implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(ContarPareceres.class);

	/**
	 * Verifica se a proposta foi aceite por mais de metade dos Peritos
	 */
	public void execute(DelegateExecution execution) throws Exception {
		
		log.info("ContarPareceres iniciado");
		
		List<Boolean> pareceres = (List<Boolean>) execution.getVariable("pareceres");
		List<String> peritos = (List<String>) execution.getVariable("peritosList");

		int count = 0;
		
		if(pareceres==null) {
			pareceres = new ArrayList<Boolean>();
		}
		
		for(Boolean b : pareceres) {
			if(b) count++;
		}
		
		boolean aprovado = false;
		
		if((double)count > ((double)peritos.size())/2) {
			aprovado = true;
		}
		
		log.info("Parecer Final: " + aprovado);
		
		execution.setVariable("resultadoParecer", aprovado);
	}

}
