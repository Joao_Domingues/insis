package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProcessarPeritos implements JavaDelegate {

	private static Log log = LogFactory.getLog(ProcessarPeritos.class);
	
	/**
	 * Processa o JSON recebido da /biblioteca/peritos, convertendo-o para uma lista de strings
	 */
	public void execute(DelegateExecution execution) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, ArrayList<String>> map = mapper.readValue(execution.getVariable("peritosRegistados").toString(), Map.class);
		
		String aux = map.get("peritos").toString();
		
		// remove []
		aux = aux.substring(1, aux.length()-1);
		
		// split by ,
		String[] peritosArray = aux.split(",");
		
		List<String> peritosList = new ArrayList<String>();
		for(String p : peritosArray) {
			// remove {}
			p = p.substring(1, p.length()-1);
			// split by =, get second string
			p = p.split("=")[1];
			peritosList.add(p);
		}
		
		log.info("Lista de peritos: " + peritosList);
		
		execution.setVariable("peritosRegistados", peritosList);

	}

}
