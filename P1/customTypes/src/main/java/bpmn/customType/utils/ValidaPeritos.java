package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ValidaPeritos implements JavaDelegate {

	private static Log log = LogFactory.getLog(ValidaPeritos.class);
	
	/**
	 * Verifica se os Peritos inseridos est�o registados no sistema
	 * Verifica tamb�m se o n�mero de peritos inseridos � v�lido
	 */
	public void execute(DelegateExecution execution) throws Exception {
		
		log.info("ValidaPeritos Servi�o");

		String peritosInseridos = (String) execution.getVariable("peritosInseridos");
		List<String> peritosRegistadosList = new ArrayList<String>();
		
		peritosRegistadosList = (List<String>) execution.getVariable("peritosRegistados");
		
		String[] peritosInseridosArray = peritosInseridos.split(";");
		
		boolean peritosValidos = true;
		
		// Verifica se todos os peritos inseridos est�o na lista dos existentes
		for(int i = 0; i < peritosInseridosArray.length; i++) {
			if(!peritosRegistadosList.contains(peritosInseridosArray[i].split("@")[0])) {
				log.info(("Perito inserido nao encontrado: " + peritosInseridosArray[i].split("@")[0]));
				peritosValidos = false;
			}
		}
		
		log.info("Os peritos inseridos sao validos? " + peritosValidos);
		
		// Verifica se utilizador atual � o Bibliotec�rio
		boolean bibliotecario = execution.getVariable("flagPeritos").toString().equals("bibliotecario");
		log.info("O utilizador atual e o bibliotecario? " + bibliotecario);
		
		// Valida o n�mero de Peritos inseridos
		if(peritosInseridosArray.length < 2) peritosValidos = false;
		if(peritosInseridosArray.length > 2 && bibliotecario) peritosValidos = false;
		log.info("Numero valido de peritos inserido? " + peritosValidos);
		
		execution.setVariable("peritosValidos", peritosValidos);
	}

}
