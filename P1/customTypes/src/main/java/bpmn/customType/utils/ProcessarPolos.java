package bpmn.customType.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ProcessarPolos implements JavaDelegate {

	private static Log log = LogFactory.getLog(ProcessarPolos.class);
	
	/**
	 * Processa o JSON recebido da /biblioteca/polos, convertendo-o para uma lista de strings
	 */
	public void execute(DelegateExecution execution) throws Exception {
		
		log.info(" -- ProcessarPolos -- ");
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, ArrayList<String>> map = mapper.readValue(execution.getVariable("polos").toString(), Map.class);
		
		String aux = map.get("polos").toString();
		
		// remove []
		aux = aux.substring(1, aux.length()-1);
		
		// split by ,
		String[] polosArray = aux.split(",");
		
		List<String> polosList = new ArrayList<String>();
		for(String p : polosArray) {
			// remove {}
			p = p.substring(1, p.length()-1);
			// split by =, get second string
			p = p.split("=")[1];
			polosList.add(p);
		}
		
		log.info("Lista de polos: " + polosList);
		
		execution.setVariable("polos", polosList);

	}

}
