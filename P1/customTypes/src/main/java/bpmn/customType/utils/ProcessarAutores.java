package bpmn.customType.utils;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ProcessarAutores implements JavaDelegate {

	/**
	 * Converte a lista de autores para ser compatível com o formato URL desejado
	 */
	public void execute(DelegateExecution execution) throws Exception {
		String autores = (String) execution.getVariable("autor");
		
		String autorURL = autores.replace(" ", "").replace(";", "&");
		
		execution.setVariable("autorURL", autorURL);
	}

}
