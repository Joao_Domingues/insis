package bpmn.customType.listeners;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ParecerListener implements TaskListener {

	private static final long serialVersionUID = 1L;

	private static final String ASSIGNEE = "assignee";
	private static final String PARECER = "parecer";
	private static final String PARECERES = "pareceres";

	private static Log log = LogFactory.getLog(ParecerListener.class);
	
	public Expression parecerExpression;
	
	/**
	 * Sempre que � conclu�da uma instance do multi-instance task dos Peritos,
	 * � preenchida uma lista de booleans com o parecer correspondente ao Perito que 
	 * a concluiu.
	 */
	public void notify(DelegateTask delegateTask) {
		List<Boolean> selectedFacts = new ArrayList<Boolean>();
		
		if (delegateTask.hasVariable(PARECERES)) {
			selectedFacts = (List<Boolean>) delegateTask.getVariable(PARECERES);
		}
		
		log.info("Parecer: " + delegateTask.getVariable(PARECER).toString());

		Boolean selectedFact = Boolean.valueOf(delegateTask.getVariable(PARECER).toString());
		String assignee = delegateTask.getVariable(ASSIGNEE, String.class);

		log.info(assignee + ": " + selectedFact);

		selectedFacts.add(selectedFact);

		delegateTask.setVariable(PARECERES, selectedFacts);

	}

}
